const Users = require("../capstone-models/users.js");
const Products = require("../capstone-models/products.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const { update } = require("../capstone-models/users.js");
const { response } = require("express");
const users = require("../capstone-models/users.js");

// Registration
module.exports.registerUser = async (reqBody) => {

    const existingUser = await Users.findOne({email: reqBody.email});
    
    if (existingUser){
        return "Email already Exists";
    }

    const newUser = new Users ({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo,
    })

    return newUser.save()
    .then((Users, error) => 
    {
        if(error){
            return "Please Try Again";
        }else{
            return "Registered Successfully!"
        }
    })
}

// Login
module.exports.loginUser = (requestBody) => {
    return Users.findOne({email: requestBody.email})
    .then(result => {
        if(result == null){
            return 'Account not Found'
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)};
            }
            else{
                return "Incorrect Password"
            }
        }
    })
}

// Getting Profile in Database
module.exports.getProfile = (usersId, gettingDataByAdmin) => {
    if(gettingDataByAdmin.isAdmin == true){
	return Users.findById(usersId)
    .then((result, err) => {
		if(err){
			    return false;
		    }
            else{
                result.password = '*****';
                return result;
            }
        })
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// // Non Admin User Checkout
module.exports.checkOut = async (request, response) => {

    const usersData = auth.decode(request.headers.authorization);

    let productToCart = await Products.findById(request.body.productId)
    .then(result => result);

    let newOrder = {
        usersId: usersData.id,
        email: usersData.email,
        subTotalAmount: productToCart.price * (request.body.quantity || 1),
        productId: request.body.productId,
        productName: productToCart.name,
        quantity: usersData.myOrders[0].products[0].quantity || request.body.quantity, // default order quantity
        stocks: productToCart.stocks,
        isAdmin: usersData.isAdmin,
        price: productToCart.price,
        totalAmount: 0
    }

    let isUserUpdated = await Users.findById(newOrder.usersId)
    .then(user => {
        if(newOrder.isAdmin == true && productToCart.stocks < request.body.quantity && productToCart.stocks <= 0){
            response.status(404).json("Only customers can order Products");
        }else {

            let subTotalSum = 0;
            user.myOrders.forEach(order =>{
                subTotalSum += Number(order.subTotalAmount);
            })

            let storedPay = subTotalSum + newOrder.subTotalAmount

            if (newOrder.quantity > productToCart.stocks) {
                response.status(400).json("Not Enough Stocks for your request");
            } else {

            user.myOrders.push({
                products: [
                    {
                        productId: newOrder.productId,
                        productName: newOrder.productName,
                        quantity: newOrder.quantity || 1 ,// default order quantity
                    },
                ],
                subTotalAmount: newOrder.subTotalAmount,
                totalAmount: storedPay
            })

            return user.save()
            .then(result => {
                return true;
            })
            .catch(error => {
                return error;
            })
        }
    }
})
    .catch(error => {
        return error;
    });

    console.log(isUserUpdated); // dapat mag True if Successful


    let isProductUpdated = await Products.findById(productToCart)
        
        .then(product => {

            if(newOrder.isAdmin == true){
                response.status(404).json("Only customers can order Products");
                }else{
                            product.orderDetails.push({
                                userEmail: newOrder.email,
                                quantity: newOrder.quantity || 1 ,// default order quantity
                            })

                            if(productToCart.stocks <= 0){
                                response.status(404).send("Out of Stock");
                            }
                            else if (productToCart.stocks < newOrder.quantity)
                                {
                                response.status(404).send("Not Enough Stocks for your request");
                            }
                            else{
                                    product.stocks -= newOrder.quantity;
                                    return product.save()
                                        .then(result => {
                                            return true;
                                        })
                                        .catch(error => {
                                            return error;
                                    })
                                }
                            }
            })
    .catch(error => {
        return error;
    });
    
        console.log(isProductUpdated); // Dapat mag True if Successful

        (isUserUpdated == true && isProductUpdated == true)?
        response.status(200).send("Thanks for Purchasing!") : response.send(false);
}

// Additional Features
// Change to Admin Status
module.exports.setStatus = async (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let newAdmin = {
        usersId: req.body.usersId
    }

    if(userData.isAdmin == true){
	
    await Users.findById(newAdmin.usersId)
    .then(result => {
                result.isAdmin = true
                result.save();
                res.send("You are now Promoted as Admin!")
        })
        .catch(err => console.log(err))
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// Retrieved Orders
module.exports.myCurrentOrders = async (req, res) => {

    const userData = auth.decode(req.headers.authorization);
  
    let guestOrders = await Users.findById(req.body.usersId)
      .then(usersId => usersId);
  
    let myDetails = {
      usersId: guestOrders,
      orders: guestOrders.myOrders,
      isAdmin: guestOrders.isAdmin,
    }
  
    if (!myDetails.isAdmin) {
      let numOrders = myDetails.orders.length;
      let totalAmount = 0;
      let orderDetails = [];
      
      myDetails.orders.forEach(order => {
        totalAmount += order.totalAmount;
        order.products.forEach(product => {
          orderDetails.push({
            "Order": order.purchasedOn,
            "ProductName": product.productName,
            "Quantity": product.quantity,
            "TotalAmount": order.totalAmount
          });
        });
      });

      let summary = {
        "Number of Orders": numOrders,
        "Total Amount": totalAmount,
        "Orders": orderDetails
      };
      res.send(summary);
    }
  }
