const express = require("express");
const router = express.Router();
const userControllers = require("../capstone-controllers/userControllers.js");
const auth = require("../auth.js");
const users = require("../capstone-models/users.js");

// Register Routes
router.post("/Register", (request, response) => {
    userControllers.registerUser(request.body)
    .then(resultFromController => response.send(resultFromController))
})

// Getting Profile Routes
router.get("/:usersId/userDetails", auth.verify, (request, response) => {
    
    const gettingDataByAdmin = {
        isAdmin : auth.decode(request.headers.authorization).isAdmin
    }
    
    userControllers.getProfile(request.params.usersId, gettingDataByAdmin)
    .then(resultFromController => 
        response.send(resultFromController))
})

// Login Routes
router.post("/Login", (request, response) => {
    userControllers.loginUser(request.body)
    .then(resultFromController =>
        response.send(resultFromController))
})

// Checkout Routes
router.post("/Checkout", auth.verify, userControllers.checkOut);

// set Status
router.post("/Promotion", auth.verify, userControllers.setStatus);

// Retrieve Authenticated User's Orders (Not Admin)
router.post("/myOrders", auth.verify, userControllers.myCurrentOrders);

module.exports = router;